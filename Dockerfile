FROM gcc 

RUN apt-get update && \
	apt-get install libboost-thread-dev libboost-regex-dev libboost-system-dev libboost-date-time-dev -y && \
	apt-get install libssl-dev

RUN curl -o /usr/bin/mc https://dl.min.io/client/mc/release/linux-amd64/mc && chmod +x /usr/bin/mc 

RUN date "+%Y-%m-%d %H:%M:%S" > BUILD-DATE.txt

